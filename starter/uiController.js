//questo gestisce il "frontend"
export default function UIController() {
    //uso una mappa appunto per mappare i campi di imput del frontend...avevo voglia di usare una mappa
    const UiMap = new Map([
        ['totalinc', document.querySelector('.budget__income--value')],
        ['totalexp', document.querySelector('.budget__expenses--value')],
        ['perc', document.querySelector('.budget__expenses--percentage')],
        ['tot', document.querySelector('.budget__value')],
        ['month', document.querySelector('.budget__title--month')],
        ['inputType', document.getElementById('addType')],
        ['inputDescription', document.getElementById('addDescription')],
        ['inputValue', document.getElementById('value')]
    ])
    
    const prova = () => console.log('provaUI')

    
        const inputs = () => inputs = {
            type: UiMap.get('inputType').value,
            description: UiMap.get('inputDescription').value,
            value: parseFloat(UiMap.get('inputValue').value)
        }

        //nb la funzione onclick sul button
        const printElement= (typeId, id, input) => {
            document.getElementById(typeId).insertAdjacentHTML("beforeend", `<div class="item clearfix" id="${id}">
                            <div class = "item__description"> ${input.description} </div>
                            <div class="right clearfix">
                                <div class="item__value">${input.value}</div>
                                <div class="item__delete">
                                    <button class = "item__delete--btn"onclick = "deleteItem(this.id)" id="${id}-del"><i class = "ion-ios-close-outline"> </i></button>
                                </div>
                            </div>
                        </div>`)
        }
        const cancelElement= (obj) => {
            obj.remove()
        }
        const updateTotalUi= (total, type) => {
            UiMap.get('total' + type).textContent = total[type]
            UiMap.get('perc').textContent = total.perc.toFixed(2).toString() + '%'
            UiMap.get('tot').textContent = '€' + total.tot.toFixed(2).toString()
        }
       const  displayMonth = function () {
            let today = new Date()
            let year = today.getFullYear()
            let month = today.getMonth()
            const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            UiMap.get('month').textContent = `${months[month]} ${year}`
        }

        clearfield: () => {
            UiMap.get('inputDescription').value = null
            UiMap.get('inputValue').value = null
            UiMap.get('inputDescription').focus()
        }
    }
