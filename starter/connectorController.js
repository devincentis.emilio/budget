import Item from './Item.js'

export default function connectorController(data, UIController) {
     //semplifico un pò
     let allItem = data.allItem
     let totals = data.totals
     let UiCtrl = UIController;
   


    const displayProva = function () {
        console.log('provaFunction')
    }
    displayProva()

   

    let x = UIController
    console.log('x: ', x);

    

   
    //insert viene invocato da onclick, l'id html del button e add_btn 
    insertItem = () => {
            let inputs = UiCtrl.inputs() //prendo gli input

            //vedo se è stato inserito il richiesto
            if (inputs.description !== "" && !isNaN(inputs.value)) {

                //inserisco nel "backend"
                let newItem = new Item(...Object.values(inputs)).addItem(allItem)
                totals.totalUpdate(newItem.type)

                //inserisco nel "frontend"
                UiCtrl.printElement(newItem.type === 'inc' ? 'incomeList' : 'expenseList', `${newItem.type}-${newItem.id}`, inputs)
                UiCtrl.updateTotalUi(totals, newItem.type)

            } else {
                console.log('errore')
            }
            UiCtrl.clearfield()

        },

        //delete viene invocato da onclick, l'id html del button e item__delete--btn, viene generato dalla insert
        deleteItem = delID => {
            [type, id] = delID.split('-')

            //cancello dal "backend"
            delItem = allItem[type].find(x => x.id === parseInt(id)) //qui c'è una ridondanza dell'uso di type che può essere migliorata
            delItem.removeItem(allItem[type])
            totals.totalUpdate(type)

            //cancello dal "frontend"
            UiCtrl.cancelElement(document.getElementById(`${type}-${id}`))
            UiCtrl.updateTotalUi(totals, type)
        }
}