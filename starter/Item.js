//avere una classe fuori da un controller non so se sia una best practice, di certo non porta avanti la mia guerra contro gli oggetti...
//ma per imparare js serve fare questo sacrificio, per ora
//conosci il tuo nemico...    

export default class Item {
    constructor(type, description, value) {
        this.type = type
        this.description = description
        this.value = value
        this.id = Date.now() //Date.now restituisce un numero che equivale ai millisecondi passati dal 1 gen 1970, lo uso per rendere unico l'oggetto
    }

    addItem(x) {
        x[this.type].push(this)
        return this

    }

    removeItem(x) {
        x.splice(x.indexOf(this), 1)
        return this
    }
}

