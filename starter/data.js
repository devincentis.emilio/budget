//questo gestisce il "backend"
export const data = {
    allItem: {
        exp: [],
        inc: []
    },
    totals: {
        exp: 0,
        inc: 0,
        perc: 0,
        tot: 0,
        totalUpdate(type) {
            const x = data.allItem[type];
            (() => x.length ? this[type] = x.reduce((x, y) => x + y.value, 0) : this[type] = 0)(this); //somma
            (() => (this.exp !== 0 || this.inc !== 0) ? this.perc = (this.exp / this.inc) * 100 : this.perc = 0)(this) //calcola percentuale
            this.tot = this.inc - this.exp
        }
    }
}
