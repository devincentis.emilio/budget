//i controller sono delle IIFE,  utilizzo il model pattern che si utilizzava in ES5 però aggiungo un pò di ES6, 
//questo è un esercizio per capire le closures, ed è propedeutico per la programmazione funzionale 
//infatti trovo molte similitudini con il form lambda su scheme

const budgetController = (function () {
    class Expense {
        constructor(id, description, value) {
            this.id = id
            this.description = description
            this.value = value
            this.percentage = -1
        }
        calculatePercentage(totalIncome) {
            if (totalIncome > 0) {
                this.percentage = (this.value / totalIncome) * 100
            } else this.percentage = -1
        }
        getPercentage() {
            return this.percentage
        }
        getPercentage() {
            return this.percentage
        }
    }



    class income {
        constructor(id, description, value) {
            this.id = id
            this.description = description
            this.value = value
        }
         calculateTotal (type) {
            let sum = 0
            data.allItem[type].map(x => sum += x.value)
            data.total[type] = sum
        }
           calculateTotal  (type) {
              let sum = 0
              data.allItem[type].map(x => sum += x.value)
              data.total[type] = sum
          }
    }
  
    const data = {
        allItem: {
            exp: [],
            inc: []
        },
        total: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentege: -1
    }
    return {
        addItem: function (type, des, val) {

            let newItem
            let ID

            //assegno l'id in questo caso assegno alla variabile ID l'ultimo id allinterno del vettore type + 1
            if (data.allItem[type].length > 0) {
                ID = data.allItem[type][data.allItem[type].length - 1].id + 1
            } else {
                ID = 0
            }

            //creo l'oggetto in base al type e lo inserisco dentro l'array
            if (type === 'exp') {
                newItem = new Expense(ID, des, val)
            } else if (type === 'inc') {
                newItem = new income(ID, des, val)
            }

            data.allItem[type].push(newItem)

            return newItem
        },

        deleteItem: function (type, id) {
            let ids = data.allItem[type].map(x => x.id)
            index = ids.indexOf(id)
            if (index !== -1) {
                data.allItem[type].splice(index, 1) //gli argomenti di splice sono l'indice e il numero di elementi da eliminare dall'array
            }

        },

        calculateBudget: function () {
            calculateTotal('exp')
            calculateTotal('inc')
            data.budget = data.total.inc - data.total.exp
            if (data.total.inc > 0) {
                data.percentege = Math.round((data.total.exp / data.total.inc) * 100)
            }
        },
        calculatePercentages: function () {
            data.allItem.exp.map(x => x.calculatePercentage(data.total.inc))
        },
        getPercentages: () => {
            const allPerc = data.allItem.exp.map(x => x.getPercentage())

            return allPerc
        },

        getBudget: () => {
            return {
                budget: data.budget,
                totalInc: data.total.inc,
                totalExp: data.total.exp,
                percentage: data.percentege
            }
        },

        testing: () => console.log(data)
    }
})()

//controller UI
const UIController = (function () {

    //creo un oggetto che contiene in questo caso gli id da dove prendiamo gli imput cosi da poter modificare solo questo oggetto se modifico l'id di uno di questi elementi
    const DOMStrings = {
        inputValue: 'value',
        inputType: 'addType',
        inputBtn: '.add__btn',
        incomeList: 'incomeList',
        expenseList: 'expenseList',
        budgetLabel: '.budget__value',
        inputDescription: 'addDescription',
        incomeLabel: '.budget__income--value',
        expenseLabel: '.budget__expenses--value',
        percentegeLabel: '.budget__expenses--percentage',
        container: '.container',
        expensePercentagesLabel: '.item__percentage',
        dateLabel: '.budget__title--month'
    }

    const formatNumber = function (num, type) {
        num = Math.abs(num)
        num = num.toFixed(2)
        let numSplit = num.split('.')

        let int = numSplit[0]
        if (int.length > 3) {
            int = int.substr(0, int.length - 3) + "," + int.substr(int.length - 3, int.length)
        }

        let dec = numSplit[1]

        return (type === 'exp' ? sign = "-" : sign = "+") + ' ' + int + '.' + dec
    }
    const nodeListForEach = function (list, callback) {
        for (let i = 0; i < list.length; i++) {
            callback(list[i], i)
        }
    }

    return {
        getInput: function () {

            return {
                type: document.getElementById(DOMStrings.inputType).value,
                description: document.getElementById(DOMStrings.inputDescription).value,
                value: parseFloat(document.getElementById(DOMStrings.inputValue).value)
            }

        },
        addListItem: function (obj, type) {
            let html, newHtml, element
            // creo un stringa html con placeholder %% questa è una soluzione molto brutta devo migliorarla
            if (type === 'inc') {
                element = DOMStrings.incomeList
                html = '<div class="item clearfix" id="inc-%id%"> <div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>';
            } else if (type === 'exp') {
                element = DOMStrings.expenseList
                html = '<div class="item clearfix" id="exp-%id%"><div class="item__description">%description%</div><div class="right clearfix"><div class="item__value">%value%</div><div class="item__percentage">21%</div><div class="item__delete"><button class="item__delete--btn"><i class="ion-ios-close-outline"></i></button></div></div></div>'
            }

            //sostituisco i campi che variano, sonon stati inseriti tra %% così da essere facilmente riconosciuti 
            newHtml = html.replace('%id%', obj.id).replace('%description%', obj.description).replace('%value%', formatNumber(obj.value, type))

            //inserisco l'html nel dom
            if (type === 'inc') {
                document.getElementById(element).insertAdjacentHTML('beforeend', newHtml)
            } else if (type === 'exp') {
                document.getElementById(element).insertAdjacentHTML('beforeend', newHtml)
            }
        },

        //cancello dal dom
        deleteListItem: function (selectorId) {
            document.getElementById(selectorId).parentNode.removeChild(document.getElementById(selectorId)) //un pò contorta come cosa ma in javascript per eliminare un elemento devo usare remoechild quindi prima selezionare il parent e poi passare in questo caso l'id del child, migliorerò qquesta cosa
        },

        //cancella i campi di input e setta il focus sulla description
        clearfield: () => [...document.querySelectorAll('#' + DOMStrings.inputDescription + ',' + '#' + DOMStrings.inputValue)].map(x => {
            x.value = ''
            x.description = ''
            document.getElementById(DOMStrings.inputDescription).focus()
        }),
        displayBudget: function (obj) {
            obj.budget > 0 ? type = 'inc' : type = 'exp'

            document.querySelector(DOMStrings.budgetLabel).textContent = formatNumber(obj.budget, type)
            document.querySelector(DOMStrings.incomeLabel).textContent = formatNumber(obj.totalInc, 'inc')
            document.querySelector(DOMStrings.expenseLabel).textContent = formatNumber(obj.totalExp, 'exp')
            if (obj.percentage > 0) {
                document.querySelector(DOMStrings.percentegeLabel).textContent = obj.percentage + '%'
            } else {
                document.querySelector(DOMStrings.percentegeLabel).textContent = '---'
            }

        },

        displayPercentages: function (percentages) {

            let fields = document.querySelectorAll(DOMStrings.expensePercentagesLabel)

            nodeListForEach(fields, function (current, index) {

                if (percentages[index] > 0) {
                    current.textContent = percentages[index] + '%'
                } else {
                    current.textContent = '---'
                }
            })

        },
        displayMonth: function () {
            let today = new Date()
            let year = today.getFullYear()
            let month = today.getMonth()
            const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            document.querySelector(DOMStrings.dateLabel).textContent = months[month] + ' ' + ' ' + year
        },
        changedType: function () {
            const fields = document.querySelectorAll(
                '.add__type' + ',' +
                '.add__description' + ',' +
                '.add__value'
            )
            nodeListForEach(fields, function (x) {
                x.classList.toggle('red-focus')
            })

            document.querySelector('.add__btn').classList.toggle('red')


        },



        getDOMStrings: () => DOMStrings
    }

})()

//controller globale, collega i 2 contrroller
const controller = (function (budgetCtrl, UICtrl) {

    const setupEventListner = function () {
        var DOM = UICtrl.getDOMStrings()
        document.querySelector(DOMStrings.inputBtn).addEventListener('click', ctrlAddItem)
        document.addEventListener('keypress', function (event) {
            if (event.keyCode === 13) {
                ctrlAddItem()
            }
        })
        document.querySelector(DOMStrings.container).addEventListener('click', ctrlDeleteItem)
        document.querySelector('.add__type').addEventListener('change', UICtrl.changedType)
    }
    const DOMStrings = UICtrl.getDOMStrings()

    const updateBudget = function () {
        //calcolo del budget
        budgetCtrl.calculateBudget()

        //return del budget

        let budget = budgetCtrl.getBudget()
        console.log('budget: ', budget)

        //mostra il budget nella ui

        UICtrl.displayBudget(budget)

    }
    const updatePercentages = function () {
        //calcolo percentuale
        budgetCtrl.calculatePercentages()

        //lettura percentuale dal controller
        let percentages = budgetCtrl.getPercentages()

        //update UI
        UICtrl.displayPercentages(percentages)

    }

    const ctrlAddItem = function () {
        let newItem
        //prendo gli imput
        let input = UICtrl.getInput()

        //aggiungo qualche controllo
        if (input.description !== "" && !isNaN(input.value) && input.value > 0) {

            //aggiungo l'elemento al budgetController
            newItem = budgetController.addItem(input.type, input.description, input.value)

            //aggiungo l'elemento alla UI
            UICtrl.addListItem(newItem, input.type)

            //svuoto i campi
            UICtrl.clearfield()

            //calcolo e aggiorno il budget e percentuale
            updateBudget()
            updatePercentages()
        }
    }
    const ctrlDeleteItem = function (event) {
        //assegno l'id ad una variabile uso parentNode (che prende il parent + vicino) 4 volte perchè è strutturato così l'html, 
        //questa soliuzione è bruttissima devo migliorarla, target restituisce l'oggetto che da inizio all'evento
        let itemID = event.target.parentNode.parentNode.parentNode.parentNode.id
        console.log('itemID: ', itemID)
        if (itemID) {
            splitID = itemID.split('-')
            let type = splitID[0]
            let ID = parseInt(splitID[1])
            //cancello da data l'item
            budgetController.deleteItem(type, ID)
            //cancello dalla ui
            UIController.deleteListItem(itemID)
            //update 
            updateBudget()
            updatePercentages()
        }

    }
    return {
        init: function () {
            UICtrl.displayMonth()
            UICtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: '---',

            })
            setupEventListner()
        }
    }

})(budgetController, UIController)

controller.init()