//avere una classe fuori da un controller non so se sia una best practice, di certo non porta avanti la mia guerra contro gli oggetti...
//ma per imparare js serve fare questo sacrificio, per ora
//conosci il tuo nemico...    

class Item {
    constructor(type, description, value) {
        this.type = type
        this.description = description
        this.value = value
        this.id = Date.now() //Date.now restituisce un numero che equivale ai millisecondi passati dal 1 gen 1970, lo uso per rendere unico l'oggetto
    }

    addItem(x) {
        x[this.type].push(this)
        return this

    }

    removeItem(x) {
        x.splice(x.indexOf(this), 1)
        return this
    }
}


//questo gestisce il "backend"
const data = {
    allItem: {
        exp: [],
        inc: []
    },
    totals: {
        exp: 0,
        inc: 0,
        perc: 0,
        tot: 0,
        totalUpdate(type) {
            const x = data.allItem[type];
            (() => x.length ? this[type] = x.reduce((x, y) => x + y.value, 0) : this[type] = 0)(this); //somma
            (() => (this.exp !== 0 || this.inc !== 0) ? this.perc = (this.exp / this.inc) * 100 : this.perc = 0)(this) //calcola percentuale
            this.tot = this.inc - this.exp
        }
    }
}


//questo gestisce il "frontend"

const UIController = function () {
    //uso una mappa appunto per mappare i campi di imput del frontend...avevo voglia di usare una mappa
    const UiMap = new Map([
        ['totalinc', document.querySelector('.budget__income--value')],
        ['totalexp', document.querySelector('.budget__expenses--value')],
        ['perc', document.querySelector('.budget__expenses--percentage')],
        ['tot', document.querySelector('.budget__value')],
        ['month', document.querySelector('.budget__title--month')],
        ['inputType', document.getElementById('addType')],
        ['inputDescription', document.getElementById('addDescription')],
        ['inputValue', document.getElementById('value')]
    ])

    prova = () => console.log('provaUI')

    return {
        inputs : () => inputs = {
            type: UiMap.get('inputType').value,
            description: UiMap.get('inputDescription').value,
            value: parseFloat(UiMap.get('inputValue').value)
        },

        //nb la funzione onclick sul button
        printElement : (typeId, id, input) => {
            document.getElementById(typeId).insertAdjacentHTML("beforeend", `<div class="item clearfix" id="${id}">
                            <div class = "item__description"> ${input.description} </div>
                            <div class="right clearfix">
                                <div class="item__value">${input.value}</div>
                                <div class="item__delete">
                                    <button class = "item__delete--btn"onclick = "deleteItem(this.id)" id="${id}-del"><i class = "ion-ios-close-outline"> </i></button>
                                </div>
                            </div>
                        </div>`)
        },
        cancelElement : (obj) => {
            obj.remove()
        },

        updateTotalUi : (total, type) => {
            UiMap.get('total' + type).textContent = total[type]
            UiMap.get('perc').textContent = total.perc.toFixed(2).toString() + '%'
            UiMap.get('tot').textContent = '€' + total.tot.toFixed(2).toString()
        },
        
        displayMonth : function () {
            let today = new Date()
            let year = today.getFullYear()
            let month = today.getMonth()
            months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            UiMap.get('month').textContent = `${months[month]} ${year}`
        },

        clearfield: () => {
            UiMap.get('inputDescription').value = null
            UiMap.get('inputValue').value = null
            UiMap.get('inputDescription').focus()
        }
    }
}


//funzione principale
const connectorController = function (data, UIController) {
    //semplifico un pò
    let allItem = data.allItem
    let totals = data.totals
    let UiCtrl = UIController;

    UiCtrl.displayMonth()

    //insert viene invocato da onclick, l'id html del button e add_btn 
    insertItem = () => {
            let inputs = UiCtrl.inputs() //prendo gli input

            //vedo se è stato inserito il richiesto
            if (inputs.description !== "" && !isNaN(inputs.value)) {

                //inserisco nel "backend"
                let newItem = new Item(...Object.values(inputs)).addItem(allItem)
                totals.totalUpdate(newItem.type)

                //inserisco nel "frontend"
                UiCtrl.printElement(newItem.type === 'inc' ? 'incomeList' : 'expenseList', `${newItem.type}-${newItem.id}`, inputs)
                UiCtrl.updateTotalUi(totals, newItem.type)

            } else {
                console.log('errore')
            }
            UiCtrl.clearfield()

        },

        //delete viene invocato da onclick, l'id html del button e item__delete--btn, viene generato dalla insert
        deleteItem = delID => {
            [type, id] = delID.split('-')

            //cancello dal "backend"
            delItem = allItem[type].find(x => x.id === parseInt(id)) //qui c'è una ridondanza dell'uso di type che può essere migliorata
            delItem.removeItem(allItem[type])
            totals.totalUpdate(type)

            //cancello dal "frontend"
            UiCtrl.cancelElement(document.getElementById(`${type}-${id}`))
            UiCtrl.updateTotalUi(totals, type)
        }
}(data, UIController())